terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.84.0"
    }
  }

  # details er injected by GitLab
  backend "http" {
  }
}

provider "azurerm" {
  features {}
}
